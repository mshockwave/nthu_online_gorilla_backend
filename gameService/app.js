var io = require('socket.io')(),
    Player = require('./Player').Player,
    express = require('express'),
    router = express.Router();

//Game Part
var Game = function(){
    this.players = {};

    this.HEIGHT = 40;
    this.WIDTH = 160;

    this.levelNum = 4;
    this.requirePlayer = 2;
    this.timeLimitMs = 20 * 1000;

    this.rectNum = 9;
    this.rectSize = 7;
    this.levelsRect = [];

    generateRect.call(this);
    //debugger;
};
Game.prototype.addPlayer = function(player){
    this.players[player.getId()] = player;
};
Game.prototype.getPlayers = function(){
    return this.players;
};
Game.prototype.getRects = function(level){
    return this.levelsRect[level - 1]
};
var generateRect = function(){
    var thiz = this;
    var i, j, k;

    var BOUNDARY_WIDTH = thiz.WIDTH - thiz.rectSize,
        BOUNDARY_HEIGHT = thiz.HEIGHT - thiz.rectSize;
    var ranges = [];

    for(k = 0; k < thiz.levelNum; k++){
        thiz.levelsRect[k] = [];

        for(i = 0; i <= thiz.HEIGHT; i++){
            ranges[i] = [];
            for(j = 0; j <= thiz.WIDTH; j++){
                ranges[i][j] = false;
            }
        }
        //debugger;

        var r, found;
        for(r = 1; r <= thiz.rectNum; r++){
            while(true){
                found = true;
                var w = Math.floor(Math.random() * BOUNDARY_WIDTH),
                    h = Math.floor(Math.random() * BOUNDARY_HEIGHT);
                //debugger;
                var itW, itH;
                for(itH = h; itH <= (h + thiz.rectSize); itH++){
                    for(itW = w; itW <= (w + thiz.rectSize); itW++){
                        if(ranges[itH][itW] === true){
                            found = false;
                            break;
                        }
                    }
                    if(found == false) break;
                }
                if(found === false) continue;

                for(itH = h; itH <= (h + thiz.rectSize); itH++){
                    for(itW = w; itW <= (w + thiz.rectSize); itW++){
                        ranges[itH][itW] = true;
                    }
                }
                //debugger;
                thiz.levelsRect[k].push({
                    x: w,
                    y: h
                });
                break;
            }
        }
    }
};

var gGame = new Game();
var gSubmitStack = [];

var getCurrentFormatTime =  function(){
    var date = new Date();

    return (  date.getFullYear().toString() + '-'
            + (date.getMonth()+1).toString() + '-'
            + date.getDate().toString() + ' '
            + date.getHours().toString() + ':'
            + date.getMinutes().toString() + ':'
            + date.getSeconds().toString() );
};

//IO Part
io.on('connection', function(socket){
    console.log('Got client connection');

    //New Player
    var player = new Player({
        id: socket.id,
        io: socket
    });
    socket.join('default');

    socket.on('userName', function(data){
        if('name' in data){
            player.setName(data['name']);
            gGame.addPlayer(player);

            var players = gGame.getPlayers();
            var names = [];
            for(var p in players){
                names.push(players[p].getName());
            }
            io.sockets.in('default').emit('playerList', {
                requireNum: gGame.requirePlayer,
                timeLimit: gGame.timeLimitMs,
                levelNum: gGame.levelNum,
                players: names
            });

            if(names.length >= gGame.requirePlayer){
                io.sockets.in('default').emit('gameStart');
            }
        }
    });
    socket.emit('userName');

    socket.on('rect', function(data){
        if('level' in data){
            var level = data['level'];
            var rects = gGame.getRects(level);

            socket.emit('rect', {
                level: level,
                size: gGame.rectSize,
                rects: rects
            });
        }
    });

    socket.on('result', function(data){
        if('count' in data && 'level' in data){
            var p = gGame.getPlayers()[socket.id];

            var score = (data['count'] < 0)? 0 : data['count'];
            p.addPassCount(score);
            console.log(p.getId() + ' add ' + data['count']);

            gSubmitStack.unshift({
                sId: gSubmitStack.length,
                time: getCurrentFormatTime(),
                name: p.getName(),
                problem: 'Level ' + data['level'],
                score: score
            });
        }
    });

});

/*
for(var t = 0; t < 10; t++){
    gSubmitStack.unshift({
        sId: gSubmitStack.length,
        time: getCurrentFormatTime(),
        name: 'test',
        problem: 'Level ' + 2,
        score: 5
    });
}
gSubmitStack.unshift({
    sId: gSubmitStack.length,
    time: getCurrentFormatTime(),
    name: 'prob',
    problem: 'Level ' + 3,
    score: 9
});
*/

//Express part
router.get('/', function(req, resp){
    if(gSubmitStack.length <= 0){
        resp.end();
    }else {
        var outStack = [];
        for(var i = 0; i < Math.min(gSubmitStack.length, 21); i++){
            outStack.push(gSubmitStack[i]);
        }

        resp.render('submit', {
            submits: outStack
        });
    }
});

module.exports.IO = io;
module.exports.Status = router;