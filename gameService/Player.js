
var player = function(initData){
    this.id = ('id' in initData)? initData['id'] : "-1";
    this.name = ('name' in initData)? initData['name'] : 'unknown';
    this.io = ('io' in initData)? initData['io'] : null;

    this.passCount = 0;
};
player.prototype.getName = function(){
    return this.name;
};
player.prototype.setName = function(name){
    this.name = name;
};
player.prototype.getId = function(){
    return this.id;
};
player.prototype.getIO = function(){
    return this.io;
};
player.prototype.addPassCount = function(deltaCount){
    this.passCount += deltaCount;
};

module.exports.Player = player;
